# mlua-tract

Lua bindings for [tract](https://github.com/sonos/tract), the tiny, no-nonsense, self-contained,
Rust-based Tensorflow and ONNX inference runtime.

[![License](http://img.shields.io/badge/Licence-MIT-blue.svg)](LICENSE)
[![Arch](https://img.shields.io/badge/Arch-aarch64%20|%20amd64%20|%20armv7-blue.svg)]()
[![Lua](https://img.shields.io/badge/Lua-5.1%20|%205.2%20|%205.3%20|%205.4%20|%20LuaJIT%20|%20LuaJIT%205.2-blue.svg)]()

## Installing

Add to your Rust project using one of MLua's features: [lua51, lua52, lua53, lua54, luajit, luajit52].

```shell
$ cargo add mlua-tract --features luajit
```

## Using

```rust
use mlua::Lua;
use mlua_tract;

let lua = Lua::new();
mlua_tract::preload(&lua)?;
let script = r#"
    local tract_onnx = require('tract.onnx')
    local model = tract_onnx.model_for_path('testdata/keras-tract-tf2-example.onnx')
    return tostring(model:input_fact(1))
"#;
let result: String = lua.load(script).eval()?; // returns: "unk__6,100,F32"
```

## Testing

```shell
$ make check
```
