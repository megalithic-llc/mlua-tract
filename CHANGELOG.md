# mlua-tract Changelog

## [0.1.2] - 2024-02-17
### Added
- [#17](https://gitlab.com/megalithic-llc/mlua-tract/-/issues/17) Support Tensor struct

### Changed
- [#16](https://gitlab.com/megalithic-llc/mlua-tract/-/issues/16) Upgrade 4 crates
- [#14](https://gitlab.com/megalithic-llc/mlua-tract/-/issues/14) Upgrade Rust from 1.74.1 → 1.76.0

### Fixed
- [#12](https://gitlab.com/megalithic-llc/mlua-tract/-/issues/12) A Mutex is not required to mutate a TypedModel or InferenceModel

## [0.1.1] - 2024-01-13
### Added
- [#7](https://gitlab.com/megalithic-llc/mlua-tract/-/issues/7) Provide an integration test demonstrating how to make a tensor out of an image
- [#6](https://gitlab.com/megalithic-llc/mlua-tract/-/issues/6) Support RunnableModel struct
- [#5](https://gitlab.com/megalithic-llc/mlua-tract/-/issues/5) Support ndarray Array4<u8> constructors: default(), zeros(), ones(), and from_shape_fn()
- [#4](https://gitlab.com/megalithic-llc/mlua-tract/-/issues/4) Support loading an ONNX model

### Changed
- [#8](https://gitlab.com/megalithic-llc/mlua-tract/-/issues/8) Upgrade mlua from 0.9.2 → 0.9.4

## [0.1.0] - 2024-01-07
### Added
- [#3](https://gitlab.com/megalithic-llc/mlua-tract/-/issues/3) Support TypedModel struct
- [#2](https://gitlab.com/megalithic-llc/mlua-tract/-/issues/2) Support InferenceModel struct
- [#1](https://gitlab.com/megalithic-llc/mlua-tract/-/issues/1) Put CI+CD in place
