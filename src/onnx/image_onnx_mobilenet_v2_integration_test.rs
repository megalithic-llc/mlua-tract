use mlua::Lua;
use std::error::Error;

// https://github.com/sonos/tract/blob/v0.20.22/examples/onnx-mobilenet-v2/src/main.rs
#[test]
fn test() -> Result<(), Box<dyn Error>> {
    let lua = Lua::new();
    crate::preload(&lua)?;
    mlua_image::preload(&lua)?;
    let script = r#"
        -- load the model
        local tract_data = require('tract.data')
        local tract_ndarray = require('tract.ndarray')
        local tract_onnx = require('tract.onnx')
        local image = require('image')
        local imageops = require('image.ops')
        
        local model tract_onnx.model_for_path('testdata/keras-tract-tf2-example.onnx')
            :into_optimized()
            :into_runnable()
        
        -- open image, resize it and make a tensor out of it
        local img = image.open('testdata/grace_hopper.jpg'):to_rgb8()
        local resized = imageops.resize(img, 224, 224, 'triangle')
        local mean_table = {0.485, 0.456, 0.406}
        local std_table = {0.229, 0.224, 0.225}
        local array = tract_ndarray.Array4:u8():from_shape_fn({1, 3, 224, 244}, function(_, c, y, x)
            local mean = mean_table[c + 1]
            local std = std_table[c + 1]
            return 0 -- TODO (resized[(x as _, y as _)][c] as f32 / 255.0 - mean) / std
        end)
        local tensor = tract_data.Tensor:from(array)
        
        -- run the model on the input
        -- TODO
    "#;
    lua.load(script).exec()?;
    Ok(())
}
