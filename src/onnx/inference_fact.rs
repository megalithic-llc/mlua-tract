use mlua::Error::RuntimeError;
use mlua::{Error, UserData, UserDataMethods};

pub(crate) struct InferenceFact {
    pub(crate) delegate: tract_onnx::prelude::InferenceFact,
}

impl UserData for InferenceFact {
    fn add_methods<'lua, M: UserDataMethods<'lua, Self>>(methods: &mut M) {
        methods.add_meta_method("__tostring", |_lua, this, ()| Ok(format!("{:?}", this.delegate)));
    }
}

impl InferenceFact {
    pub fn parse(_s: String) -> Result<InferenceFact, Error> {
        Err(RuntimeError("NIY".to_string())) // TODO
    }
}
