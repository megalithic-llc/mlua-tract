use super::{RunnableModel, TypedFact};
use mlua::{AnyUserData, Error, FromLua, UserData, UserDataMethods};

pub(crate) struct TypedModel {
    pub(crate) delegate: tract_onnx::prelude::TypedModel,
}

impl UserData for TypedModel {
    fn add_methods<'lua, M: UserDataMethods<'lua, Self>>(methods: &mut M) {
        methods.add_method(
            "concretize_symbols",
            |_lua, _this, _values: mlua::Value| -> Result<(), Error> { Err(Error::RuntimeError("NIY".to_string())) },
        );
        methods.add_function("declutter", |_lua, ud: AnyUserData| {
            let mut this = ud.borrow_mut::<Self>()?;
            this.delegate
                .declutter()
                .map_err(|err| Error::RuntimeError(err.to_string()))?;
            Ok(())
        });
        methods.add_method("input_count", |_lua, this, ()| Ok(this.delegate.inputs.len()));
        methods.add_method("input_fact", |_lua, this, index: usize| {
            let typed_fact = this
                .delegate
                .input_fact(index - 1)
                .map_err(|err| Error::RuntimeError(err.to_string()))?
                .clone();
            Ok(TypedFact { delegate: typed_fact })
        });
        methods.add_method("input_name", |_lua, this, index: usize| {
            Ok(this.delegate.node(index - 1).name.clone())
        });
        methods.add_method("into_decluttered", |_lua, this, ()| {
            let typed_model_delegate = this
                .delegate
                .clone()
                .into_decluttered()
                .map_err(|err| Error::RuntimeError(err.to_string()))?;
            Ok(TypedModel {
                delegate: typed_model_delegate,
            })
        });
        methods.add_method("into_optimized", |_lua, this, ()| {
            let typed_model_delegate = this
                .delegate
                .clone()
                .into_optimized()
                .map_err(|err| Error::RuntimeError(err.to_string()))?;
            Ok(TypedModel {
                delegate: typed_model_delegate,
            })
        });
        methods.add_method("into_runnable", |_lua, this, ()| -> Result<RunnableModel, Error> {
            let runnable_model_delegate = this
                .delegate
                .clone()
                .into_runnable()
                .map_err(|err| Error::RuntimeError(err.to_string()))?;
            Ok(RunnableModel {
                _delegate: runnable_model_delegate,
            })
        });
        methods.add_function("optimize", |_lua, ud: AnyUserData| {
            let mut this = ud.borrow_mut::<Self>()?;
            this.delegate
                .optimize()
                .map_err(|err| Error::RuntimeError(err.to_string()))?;
            Ok(())
        });
        methods.add_method("output_count", |_lua, this, ()| Ok(this.delegate.outputs.len()));
        methods.add_method("output_fact", |_lua, this, index: usize| {
            let typed_fact_delegate = this
                .delegate
                .output_fact(index - 1)
                .map_err(|err| Error::RuntimeError(err.to_string()))?
                .clone();
            Ok(super::TypedFact {
                delegate: typed_fact_delegate,
            })
        });
        methods.add_method("output_name", |_lua, this, index: usize| {
            let node = this.delegate.outputs[index - 1].node;
            let name: String = this.delegate.node(node).name.to_string();
            Ok(name)
        });
        methods.add_method(
            "profile_json",
            |_lua, _this, _inputs: mlua::Value| -> Result<String, Error> {
                Err(Error::RuntimeError("NIY".to_string()))
            },
        );
        methods.add_method("property", |_lua, _this, _name: String| -> Result<String, Error> {
            Err(Error::RuntimeError("NIY".to_string()))
        });
        methods.add_method("property_keys", |_lua, this, ()| {
            let property_keys: Vec<String> = this.delegate.properties.iter().map(|x| x.0.to_string()).collect();
            Ok(property_keys)
        });
        methods.add_method("pulse", |lua, _this, args: mlua::MultiValue| -> Result<(), Error> {
            let _symbol: String = String::from_lua(args[0].clone(), lua)?;
            // TODO let pulse =
            Err(Error::RuntimeError("NIY".to_string()))
        });
        methods.add_method(
            "set_output_names",
            |_lua, _this, _names: Vec<String>| -> Result<(), Error> { Err(Error::RuntimeError("NIY".to_string())) },
        );
    }
}

#[cfg(test)]
mod tests {
    use mlua::Lua;
    use std::error::Error;

    #[test]
    fn declutter() -> Result<(), Box<dyn Error>> {
        let lua = Lua::new();
        crate::onnx::preload(&lua)?;
        let script = r#"
            local tract_onnx = require('tract.onnx')
            local model = tract_onnx.model_for_path('testdata/keras-tract-tf2-example.onnx'):into_optimized()
            model:declutter()
        "#;
        lua.load(script).exec()?;
        Ok(())
    }

    #[test]
    fn input_count() -> Result<(), Box<dyn Error>> {
        let lua = Lua::new();
        crate::onnx::preload(&lua)?;
        let script = r#"
            local tract_onnx = require('tract.onnx')
            local model = tract_onnx.model_for_path('testdata/keras-tract-tf2-example.onnx'):into_optimized()
            return model:input_count()
        "#;
        let result: usize = lua.load(script).eval()?;
        assert_eq!(result, 1);
        Ok(())
    }

    #[test]
    fn input_fact() -> Result<(), Box<dyn Error>> {
        let lua = Lua::new();
        crate::onnx::preload(&lua)?;
        let script = r#"
            local tract_onnx = require('tract.onnx')
            local model = tract_onnx.model_for_path('testdata/keras-tract-tf2-example.onnx'):into_optimized()
            return tostring(model:input_fact(1))
        "#;
        let result: String = lua.load(script).eval()?;
        assert_eq!(result, "unk__6,100,F32");
        Ok(())
    }

    #[test]
    fn input_name() -> Result<(), Box<dyn Error>> {
        let lua = Lua::new();
        crate::onnx::preload(&lua)?;
        let script = r#"
            local tract_onnx = require('tract.onnx')
            local model = tract_onnx.model_for_path('testdata/keras-tract-tf2-example.onnx'):into_optimized()
            return model:input_name(1)
        "#;
        let result: String = lua.load(script).eval()?;
        assert_eq!(result, "main_input_input");
        Ok(())
    }

    #[test]
    fn into_decluttered() -> Result<(), Box<dyn Error>> {
        let lua = Lua::new();
        crate::onnx::preload(&lua)?;
        let script = r#"
            local tract_onnx = require('tract.onnx')
            local model = tract_onnx.model_for_path('testdata/keras-tract-tf2-example.onnx'):into_optimized()
            return model:into_decluttered():input_name(1)
        "#;
        let result: String = lua.load(script).eval()?;
        assert_eq!(result, "main_input_input");
        Ok(())
    }

    #[test]
    fn into_optimized() -> Result<(), Box<dyn Error>> {
        let lua = Lua::new();
        crate::onnx::preload(&lua)?;
        let script = r#"
            local tract_onnx = require('tract.onnx')
            local model = tract_onnx.model_for_path('testdata/keras-tract-tf2-example.onnx'):into_optimized()
            return model:into_optimized():input_name(1)
        "#;
        let result: String = lua.load(script).eval()?;
        assert_eq!(result, "main_input_input");
        Ok(())
    }

    #[test]
    fn into_runnable() -> Result<(), Box<dyn Error>> {
        let lua = Lua::new();
        crate::onnx::preload(&lua)?;
        let script = r#"
            local tract_onnx = require('tract.onnx')
            local model = tract_onnx.model_for_path('testdata/keras-tract-tf2-example.onnx'):into_optimized()
            model:into_runnable()
        "#;
        lua.load(script).exec()?;
        Ok(())
    }

    #[test]
    fn optimize() -> Result<(), Box<dyn Error>> {
        let lua = Lua::new();
        crate::onnx::preload(&lua)?;
        let script = r#"
            local tract_onnx = require('tract.onnx')
            local model = tract_onnx.model_for_path('testdata/keras-tract-tf2-example.onnx'):into_optimized()
            model:optimize()
        "#;
        lua.load(script).exec()?;
        Ok(())
    }

    #[test]
    fn output_count() -> Result<(), Box<dyn Error>> {
        let lua = Lua::new();
        crate::onnx::preload(&lua)?;
        let script = r#"
            local tract_onnx = require('tract.onnx')
            local model = tract_onnx.model_for_path('testdata/keras-tract-tf2-example.onnx'):into_optimized()
            return model:output_count()
        "#;
        let result: usize = lua.load(script).eval()?;
        assert_eq!(result, 1);
        Ok(())
    }

    #[test]
    fn output_fact() -> Result<(), Box<dyn Error>> {
        let lua = Lua::new();
        crate::onnx::preload(&lua)?;
        let script = r#"
            local tract_onnx = require('tract.onnx')
            local model = tract_onnx.model_for_path('testdata/keras-tract-tf2-example.onnx'):into_optimized()
            return tostring(model:output_fact(1))
        "#;
        let result: String = lua.load(script).eval()?;
        assert_eq!(result, "unk__6,1,F32");
        Ok(())
    }

    #[test]
    fn output_name() -> Result<(), Box<dyn Error>> {
        let lua = Lua::new();
        crate::onnx::preload(&lua)?;
        let script = r#"
            local tract_onnx = require('tract.onnx')
            local model = tract_onnx.model_for_path('testdata/keras-tract-tf2-example.onnx'):into_optimized()
            return model:output_name(1)
        "#;
        let result: String = lua.load(script).eval()?;
        assert_eq!(result, "sequential/dense_1/Sigmoid");
        Ok(())
    }

    #[test]
    fn property_keys() -> Result<(), Box<dyn Error>> {
        let lua = Lua::new();
        crate::onnx::preload(&lua)?;
        let script = r#"
            local tract_onnx = require('tract.onnx')
            local model = tract_onnx.model_for_path('testdata/keras-tract-tf2-example.onnx'):into_optimized()
            return model:property_keys()
        "#;
        let result: Vec<String> = lua.load(script).eval()?;
        assert!(result.is_empty());
        Ok(())
    }
}
