use mlua::{Error, UserData, UserDataMethods};
use tract_core::prelude::{Graph, TypedFact, TypedOp};

type RunnableModelDelegateType =
    tract_onnx::prelude::RunnableModel<TypedFact, Box<dyn TypedOp>, Graph<TypedFact, Box<dyn TypedOp>>>;

pub struct RunnableModel {
    pub(crate) _delegate: RunnableModelDelegateType,
}

impl UserData for RunnableModel {
    fn add_methods<'lua, M: UserDataMethods<'lua, Self>>(methods: &mut M) {
        methods.add_method("run", |_lua, _this, _inputs: mlua::Value| -> Result<String, Error> {
            Err(Error::RuntimeError("NIY".to_string()))
        });
    }
}
