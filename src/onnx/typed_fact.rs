use mlua::{UserData, UserDataMethods};

pub(crate) struct TypedFact {
    pub(crate) delegate: tract_onnx::prelude::TypedFact,
}

impl UserData for TypedFact {
    fn add_methods<'lua, M: UserDataMethods<'lua, Self>>(methods: &mut M) {
        methods.add_meta_method("__tostring", |_lua, this, ()| Ok(format!("{:?}", this.delegate)));
    }
}
