use super::InferenceModel;
use mlua::{Error, Lua};
use tract_onnx::prelude::*;

pub(super) fn handle(_lua: &Lua, path: String) -> Result<InferenceModel, Error> {
    let model = tract_onnx::onnx()
        .model_for_path(path)
        .map_err(|err| Error::RuntimeError(err.to_string()))?;
    Ok(InferenceModel { delegate: model })
}

#[cfg(test)]
mod tests {
    use mlua::Lua;
    use std::error::Error;

    #[test]
    fn model_for_non_existent_path() -> Result<(), Box<dyn Error>> {
        let lua = Lua::new();
        crate::onnx::preload(&lua)?;
        let script = r#"
            local tract_onnx = require('tract.onnx')
            return tract_onnx.model_for_path('/tmp/_non_existent_abc_')
        "#;
        let result: mlua::Result<mlua::Value> = lua.load(script).eval();
        assert!(result.is_err());
        Ok(())
    }

    #[test]
    fn model_for_path() -> Result<(), Box<dyn Error>> {
        let lua = Lua::new();
        crate::onnx::preload(&lua)?;
        let script = r#"
            local tract_onnx = require('tract.onnx')
            return tract_onnx.model_for_path('testdata/keras-tract-tf2-example.onnx')
        "#;
        let result: mlua::Result<mlua::Value> = lua.load(script).eval();
        assert!(result.is_ok());
        Ok(())
    }
}
