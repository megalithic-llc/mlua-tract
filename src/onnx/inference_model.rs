use super::{InferenceFact, TypedModel};
use mlua::{AnyUserData, Error, UserData, UserDataMethods};
use tract_onnx::prelude::*;

pub(crate) struct InferenceModel {
    pub(crate) delegate: tract_onnx::prelude::InferenceModel,
}

impl UserData for InferenceModel {
    fn add_methods<'lua, M: UserDataMethods<'lua, Self>>(methods: &mut M) {
        methods.add_function("analyse", |_lua, ud: AnyUserData| {
            let mut this = ud.borrow_mut::<Self>()?;
            this.delegate
                .analyse(false)
                .map_err(|err| Error::RuntimeError(err.to_string()))?;
            Ok(())
        });
        methods.add_method("fact", |_lua, _this, s: String| InferenceFact::parse(s));
        methods.add_method("input_count", |_lua, this, ()| Ok(this.delegate.inputs.len()));
        methods.add_method("input_fact", |_lua, this, index: usize| {
            let delegate = this
                .delegate
                .input_fact(index - 1)
                .map_err(|err| Error::RuntimeError(err.to_string()))?
                .clone();
            Ok(InferenceFact { delegate })
        });
        methods.add_method("input_name", |_lua, this, index: usize| {
            Ok(this.delegate.node(index - 1).name.clone())
        });
        methods.add_method("into_analysed", |_lua, this, ()| {
            let mut delegate = this.delegate.clone();
            delegate
                .analyse(false)
                .map_err(|err| Error::RuntimeError(err.to_string()))?;
            Ok(InferenceModel { delegate })
        });
        methods.add_method("into_optimized", |_lua, this, ()| {
            let typed_model_delegate = this
                .delegate
                .clone()
                .into_optimized()
                .map_err(|err| Error::RuntimeError(err.to_string()))?;
            Ok(TypedModel {
                delegate: typed_model_delegate,
            })
        });
        methods.add_method("into_typed", |_lua, this, ()| {
            let typed_model_delegate = this
                .delegate
                .clone()
                .into_typed()
                .map_err(|err| Error::RuntimeError(err.to_string()))?;
            Ok(TypedModel {
                delegate: typed_model_delegate,
            })
        });
        methods.add_method("output_count", |_lua, this, ()| Ok(this.delegate.outputs.len()));
        methods.add_method("output_fact", |_lua, this, index: usize| {
            let delegate = this
                .delegate
                .output_fact(index - 1)
                .map_err(|err| Error::RuntimeError(err.to_string()))?
                .clone();
            Ok(InferenceFact { delegate })
        });
        methods.add_method("output_name", |_lua, this, index: usize| {
            let node = this.delegate.outputs[index - 1].node;
            let name: String = this.delegate.node(node).name.to_string();
            Ok(name)
        });
        methods.add_function(
            "set_input_fact",
            |_lua, (ud, index, fact_as_string): (AnyUserData, usize, String)| {
                let mut this = ud.borrow_mut::<Self>()?;
                let inference_fact = InferenceFact::parse(fact_as_string)?;
                this.delegate
                    .set_input_fact(index - 1, inference_fact.delegate)
                    .map_err(|err| Error::RuntimeError(err.to_string()))?;
                Ok(())
            },
        );
    }
}

#[cfg(test)]
mod tests {
    use mlua::Lua;
    use std::error::Error;

    #[test]
    fn analyse() -> Result<(), Box<dyn Error>> {
        let lua = Lua::new();
        crate::onnx::preload(&lua)?;
        let script = r#"
            local tract_onnx = require('tract.onnx')
            local model = tract_onnx.model_for_path('testdata/keras-tract-tf2-example.onnx')
            return model:analyse()
        "#;
        let result: mlua::Value = lua.load(script).eval()?;
        assert_eq!(result, mlua::Nil);
        Ok(())
    }

    #[test]
    #[ignore] // TODO implement
    fn fact() -> Result<(), Box<dyn Error>> {
        let lua = Lua::new();
        crate::onnx::preload(&lua)?;
        let script = r#"
            local tract_onnx = require('tract.onnx')
            local model = tract_onnx.model_for_path('testdata/keras-tract-tf2-example.onnx')
            return tostring(model:fact('1,224,224,3,f32'))
        "#;
        let result: String = lua.load(script).eval()?;
        assert_eq!(result, "1,224,224,3,f32");
        Ok(())
    }

    #[test]
    fn input_count() -> Result<(), Box<dyn Error>> {
        let lua = Lua::new();
        crate::onnx::preload(&lua)?;
        let script = r#"
            local tract_onnx = require('tract.onnx')
            local model = tract_onnx.model_for_path('testdata/keras-tract-tf2-example.onnx')
            return model:input_count()
        "#;
        let result: usize = lua.load(script).eval()?;
        assert_eq!(result, 1);
        Ok(())
    }

    #[test]
    fn input_fact() -> Result<(), Box<dyn Error>> {
        let lua = Lua::new();
        crate::onnx::preload(&lua)?;
        let script = r#"
            local tract_onnx = require('tract.onnx')
            local model = tract_onnx.model_for_path('testdata/keras-tract-tf2-example.onnx')
            return tostring(model:input_fact(1))
        "#;
        let result: String = lua.load(script).eval()?;
        assert_eq!(result, "unk__6,100,F32");
        Ok(())
    }

    #[test]
    fn input_name() -> Result<(), Box<dyn Error>> {
        let lua = Lua::new();
        crate::onnx::preload(&lua)?;
        let script = r#"
            local tract_onnx = require('tract.onnx')
            local model = tract_onnx.model_for_path('testdata/keras-tract-tf2-example.onnx')
            return model:input_name(1)
        "#;
        let result: String = lua.load(script).eval()?;
        assert_eq!(result, "main_input_input");
        Ok(())
    }

    #[test]
    fn into_optimized() -> Result<(), Box<dyn Error>> {
        let lua = Lua::new();
        crate::onnx::preload(&lua)?;
        let script = r#"
            local tract_onnx = require('tract.onnx')
            local model = tract_onnx.model_for_path('testdata/keras-tract-tf2-example.onnx')
            return model:into_optimized()
        "#;
        let result: mlua::Result<mlua::Value> = lua.load(script).eval();
        assert!(result.is_ok());
        Ok(())
    }

    #[test]
    fn into_typed() -> Result<(), Box<dyn Error>> {
        let lua = Lua::new();
        crate::onnx::preload(&lua)?;
        let script = r#"
            local tract_onnx = require('tract.onnx')
            local model = tract_onnx.model_for_path('testdata/keras-tract-tf2-example.onnx')
            return model:into_typed()
        "#;
        let result: mlua::Result<mlua::Value> = lua.load(script).eval();
        assert!(result.is_ok());
        Ok(())
    }

    #[test]
    fn output_count() -> Result<(), Box<dyn Error>> {
        let lua = Lua::new();
        crate::onnx::preload(&lua)?;
        let script = r#"
            local tract_onnx = require('tract.onnx')
            local model = tract_onnx.model_for_path('testdata/keras-tract-tf2-example.onnx')
            return model:output_count()
        "#;
        let result: usize = lua.load(script).eval()?;
        assert_eq!(result, 1);
        Ok(())
    }

    #[test]
    fn output_fact() -> Result<(), Box<dyn Error>> {
        let lua = Lua::new();
        crate::onnx::preload(&lua)?;
        let script = r#"
            local tract_onnx = require('tract.onnx')
            local model = tract_onnx.model_for_path('testdata/keras-tract-tf2-example.onnx')
            return tostring(model:output_fact(1))
        "#;
        let result: String = lua.load(script).eval()?;
        assert_eq!(result, "..,?");
        Ok(())
    }

    #[test]
    fn output_name() -> Result<(), Box<dyn Error>> {
        let lua = Lua::new();
        crate::onnx::preload(&lua)?;
        let script = r#"
            local tract_onnx = require('tract.onnx')
            local model = tract_onnx.model_for_path('testdata/keras-tract-tf2-example.onnx')
            return model:output_name(1)
        "#;
        let result: String = lua.load(script).eval()?;
        assert_eq!(result, "sequential/dense_1/Sigmoid");
        Ok(())
    }

    #[test]
    #[ignore] // TODO implement
    fn set_input_fact() -> Result<(), Box<dyn Error>> {
        let lua = Lua::new();
        crate::onnx::preload(&lua)?;
        let script = r#"
            local tract_onnx = require('tract.onnx')
            local model = tract_onnx.model_for_path('testdata/keras-tract-tf2-example.onnx')
            model:set_input_fact(1, '1,3,224,244,f32')
            return tostring(model:input_fact(1))
        "#;
        let result: String = lua.load(script).eval()?;
        assert_eq!(result, "1,3,224,244,f32");
        Ok(())
    }
}
