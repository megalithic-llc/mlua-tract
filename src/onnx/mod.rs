mod inference_fact;
mod inference_model;
mod model_for_path;
mod runnable_model;
mod typed_fact;
mod typed_model;

pub(crate) use inference_fact::InferenceFact;
pub(crate) use inference_model::InferenceModel;
pub(crate) use runnable_model::RunnableModel;
pub(crate) use typed_fact::TypedFact;
pub(crate) use typed_model::TypedModel;

use mlua::{Error, Lua, Table};

pub fn preload(lua: &Lua) -> Result<(), Error> {
    // Configure module table
    let module = lua.create_table()?;
    module.set("model_for_path", lua.create_function(model_for_path::handle)?)?;

    // Preload module
    let globals = lua.globals();
    let package = globals.get::<_, Table>("package")?;
    let loaded = package.get::<_, Table>("loaded")?;
    loaded.set("tract.onnx", module)?;

    Ok(())
}

#[cfg(test)]
mod image_onnx_mobilenet_v2_integration_test;

#[cfg(test)]
mod tests {
    use mlua::{Lua, Table};
    use std::error::Error;

    #[test]
    fn preload() -> Result<(), Box<dyn Error>> {
        let lua = Lua::new();
        super::preload(&lua)?;
        let _module: Table = lua.load("return require('tract.onnx')").eval()?;
        Ok(())
    }
}
