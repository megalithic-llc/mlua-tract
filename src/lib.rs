pub mod data;
pub mod ndarray;
pub mod onnx;

use mlua::{Error, Lua};

/// Preload the 'tract' Lua module and its submodules.
///
/// Example:
/// ```rust
/// use mlua::Lua;
/// let lua = Lua::new();
/// mlua_tract::preload(&lua).unwrap();
/// ```
pub fn preload(lua: &Lua) -> Result<(), Error> {
    // Preload modules
    data::preload(lua)?;
    ndarray::preload(lua)?;
    onnx::preload(lua)?;

    Ok(())
}
