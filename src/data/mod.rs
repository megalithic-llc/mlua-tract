use crate::data::tensor_factory::TensorFactory;
use mlua::{Error, Lua, Table};

mod tensor;
mod tensor_factory;

pub(crate) fn preload(lua: &Lua) -> Result<(), Error> {
    // Configure module table
    let module = lua.create_table()?;
    module.set("Tensor", lua.create_userdata(TensorFactory {})?)?;

    // Preload module
    let globals = lua.globals();
    let package = globals.get::<_, Table>("package")?;
    let loaded = package.get::<_, Table>("loaded")?;
    loaded.set("tract.data", module)?;

    Ok(())
}
