use super::tensor::Tensor;
use crate::ndarray::array4_u8::Array4U8;
use mlua::{AnyUserData, UserData, UserDataMethods};

pub(crate) struct TensorFactory {}

impl UserData for TensorFactory {
    fn add_methods<'lua, M: UserDataMethods<'lua, Self>>(methods: &mut M) {
        methods.add_method("default", |_lua, _this, ()| {
            let delegate = tract_data::prelude::Tensor::default();
            Ok(Tensor { delegate })
        });
        methods.add_method("from", |_lua, _this, ud: AnyUserData| {
            let datum = ud.borrow::<Array4U8>()?;
            let delegate = tract_data::prelude::Tensor::from(datum.delegate.clone());
            Ok(Tensor { delegate })
        });
    }
}
