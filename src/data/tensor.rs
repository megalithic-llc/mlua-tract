use mlua::{AnyUserData, Error, UserData, UserDataMethods};

pub(crate) struct Tensor {
    pub delegate: tract_data::prelude::Tensor,
}

impl UserData for Tensor {
    fn add_methods<'lua, M: UserDataMethods<'lua, Self>>(methods: &mut M) {
        methods.add_function("broadcast_to_rank", |_lua, (this_ud, rank): (AnyUserData, usize)| {
            let mut this = this_ud.borrow_mut::<Self>()?;
            this.delegate
                .broadcast_to_rank(rank)
                .map_err(|err| Error::RuntimeError(err.to_string()))?;
            Ok(())
        });
        methods.add_method("clone", |_lua, this, ()| {
            let delegate = this.delegate.clone();
            Ok(Tensor { delegate })
        });
        methods.add_method("deep_clone", |_lua, this, ()| {
            let delegate = this.delegate.deep_clone();
            Ok(Tensor { delegate })
        });
        methods.add_method("dump", |_lua, this, force_full: bool| {
            let result = this
                .delegate
                .dump(force_full)
                .map_err(|err| Error::RuntimeError(err.to_string()))?;
            Ok(result)
        });
        methods.add_function("eq", |_lua, (this_ud, other_ud): (AnyUserData, AnyUserData)| {
            let this = this_ud.borrow::<Self>()?;
            let other = other_ud.borrow::<Self>()?;
            Ok(this.delegate.eq(&other.delegate))
        });
        methods.add_function("insert_axis", |_lua, (this_ud, axis): (AnyUserData, usize)| {
            let mut this = this_ud.borrow_mut::<Self>()?;
            this.delegate
                .insert_axis(axis)
                .map_err(|err| Error::RuntimeError(err.to_string()))?;
            Ok(())
        });
        methods.add_method("is_uniform", |_lua, this, ()| Ok(this.delegate.is_uniform()));
        methods.add_method("is_zero", |_lua, this, ()| {
            let result = this
                .delegate
                .is_zero()
                .map_err(|err| Error::RuntimeError(err.to_string()))?;
            Ok(result)
        });
        methods.add_method("len", |_lua, this, ()| Ok(this.delegate.len()));
        methods.add_method("nth", |_lua, this, nth: usize| {
            let delegate = this
                .delegate
                .nth(nth - 1)
                .map_err(|err| Error::RuntimeError(err.to_string()))?;
            Ok(Tensor { delegate })
        });
        methods.add_method("rank", |_lua, this, ()| Ok(this.delegate.rank()));
        methods.add_function("remove_axis", |_lua, (this_ud, axis): (AnyUserData, usize)| {
            let mut this = this_ud.borrow_mut::<Self>()?;
            this.delegate
                .remove_axis(axis)
                .map_err(|err| Error::RuntimeError(err.to_string()))?;
            Ok(())
        });
        methods.add_method("shape", |lua, this, ()| {
            let dimensions = this.delegate.shape();
            let retval = lua.create_table()?;
            for dimension in dimensions {
                retval.raw_push(*dimension)?;
            }
            Ok(retval)
        });
        methods.add_method("slice", |_lua, this, (axis, start, end): (usize, usize, usize)| {
            let delegate = this
                .delegate
                .slice(axis, start, end)
                .map_err(|err| Error::RuntimeError(err.to_string()))?;
            Ok(Tensor { delegate })
        });
        methods.add_method("strides", |lua, this, ()| {
            let dimensions = this.delegate.strides();
            let retval = lua.create_table()?;
            for dimension in dimensions {
                retval.raw_push(*dimension)?;
            }
            Ok(retval)
        });
    }
}
