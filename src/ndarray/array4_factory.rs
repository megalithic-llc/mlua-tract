use crate::ndarray::array4_u8_factory::Array4U8Factory;
use mlua::{UserData, UserDataMethods};

pub(crate) struct Array4Factory {}

impl UserData for Array4Factory {
    fn add_methods<'lua, M: UserDataMethods<'lua, Self>>(methods: &mut M) {
        methods.add_method("u8", |_lua, _this, ()| Ok(Array4U8Factory {}));
    }
}

#[cfg(test)]
mod tests {
    use mlua::Lua;
    use std::error::Error;

    #[test]
    fn u8() -> Result<(), Box<dyn Error>> {
        let lua = Lua::new();
        super::super::preload(&lua)?;
        let script = r#"
            local ndarray = require('tract.ndarray')
            ndarray.Array4:u8()
        "#;
        lua.load(script).exec()?;
        Ok(())
    }
}
