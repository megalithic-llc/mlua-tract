use mlua::{UserData, UserDataMethods};

pub(crate) struct Array4U8 {
    pub delegate: tract_core::ndarray::Array4<u8>,
}

impl UserData for Array4U8 {
    fn add_methods<'lua, M: UserDataMethods<'lua, Self>>(methods: &mut M) {
        methods.add_method("is_empty", |_lua, this, ()| Ok(this.delegate.is_empty()));
        methods.add_method("len", |_lua, this, ()| Ok(this.delegate.len()));
        methods.add_method("ndim", |_lua, this, ()| Ok(this.delegate.ndim()));
    }
}

#[cfg(test)]
mod tests {
    use mlua::Lua;
    use std::error::Error;

    #[test]
    fn is_empty() -> Result<(), Box<dyn Error>> {
        let lua = Lua::new();
        super::super::preload(&lua)?;
        let script = r#"
            local ndarray = require('tract.ndarray')
            return ndarray.Array4:u8():ones({3, 4, 5, 6}):is_empty(), ndarray.Array4:u8():ones({0, 0, 0, 0}):is_empty() 
        "#;
        let (empty1, empty2): (bool, bool) = lua.load(script).eval()?;
        assert_eq!(empty1, false);
        assert_eq!(empty2, true);
        Ok(())
    }

    #[test]
    fn len() -> Result<(), Box<dyn Error>> {
        let lua = Lua::new();
        super::super::preload(&lua)?;
        let script = r#"
            local ndarray = require('tract.ndarray')
            return ndarray.Array4:u8():zeros({3, 4, 5, 6}):len()
        "#;
        let n: usize = lua.load(script).eval()?;
        assert_eq!(n, 360);
        Ok(())
    }

    #[test]
    fn ndim() -> Result<(), Box<dyn Error>> {
        let lua = Lua::new();
        super::super::preload(&lua)?;
        let script = r#"
            local ndarray = require('tract.ndarray')
            return ndarray.Array4:u8():zeros({3, 4, 5, 6}):ndim()
        "#;
        let ndim: usize = lua.load(script).eval()?;
        assert_eq!(ndim, 4);
        Ok(())
    }
}
