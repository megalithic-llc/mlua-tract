//! Lua bindings for the `tract.ndarray` package.
//!
//! Sample usage
//! ```lua
//! local ndarray = require('tract.ndarray')
//! local temperature = ndarray.Array3:f64():zeros({3, 4, 5})
//! ```

pub(crate) mod array4_factory;
pub(crate) mod array4_u8;
pub(crate) mod array4_u8_factory;

use crate::ndarray::array4_factory::Array4Factory;
use mlua::{Error, Lua, Table};

pub(crate) fn preload(lua: &Lua) -> Result<(), Error> {
    // Configure module table
    let module = lua.create_table()?;
    module.set("Array4", lua.create_userdata(Array4Factory {})?)?;

    // Preload module
    let globals = lua.globals();
    let package = globals.get::<_, Table>("package")?;
    let loaded = package.get::<_, Table>("loaded")?;
    loaded.set("tract.ndarray", module)?;

    Ok(())
}

#[cfg(test)]
mod tests {
    use mlua::{Lua, Table};
    use std::error::Error;

    #[test]
    fn preload() -> Result<(), Box<dyn Error>> {
        let lua = Lua::new();
        super::preload(&lua)?;
        let module: Table = lua.load("return require('tract.ndarray')").eval()?;
        assert!(module.contains_key("Array4")?);
        Ok(())
    }
}
