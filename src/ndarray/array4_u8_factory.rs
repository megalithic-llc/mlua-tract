use crate::ndarray::array4_u8::Array4U8;
use mlua::{Error, FromLua, Function, MultiValue, Table, UserData, UserDataMethods};
use tract_core::ndarray::{Array, Array4, Ix4};

pub(crate) struct Array4U8Factory {}

fn dimensions_for_table(t: &Table) -> Result<[usize; 4], Error> {
    if t.len()? != 4 {
        return Err(Error::RuntimeError(
            "Shape arg must be a table with 4 elements".to_string(),
        ));
    }
    let dimensions = t.clone().sequence_values().collect::<Result<Vec<usize>, Error>>()?;
    let dimensions: [usize; 4] = dimensions.try_into().expect("slice expected 4 elements");
    Ok(dimensions)
}

impl UserData for Array4U8Factory {
    fn add_methods<'lua, M: UserDataMethods<'lua, Self>>(methods: &mut M) {
        methods.add_method("default", |_lua, _this, shape: Table| {
            let dimensions = dimensions_for_table(&shape)?;
            let delegate: Array<u8, Ix4> = Array4::default(dimensions);
            Ok(Array4U8 { delegate })
        });
        methods.add_method("from_shape_fn", |lua, _this, args: MultiValue| {
            let shape = Table::from_lua(args[0].clone(), lua)?;
            let f = Function::from_lua(args[1].clone(), lua)?;
            let dimensions = dimensions_for_table(&shape)?;
            let delegate: Array<u8, Ix4> = Array4::from_shape_fn(dimensions, |(i, j, k, l)| {
                let v: u8 = f.call((i + 1, j + 1, k + 1, l + 1)).unwrap();
                v
            });
            Ok(Array4U8 { delegate })
        });
        methods.add_method("ones", |_lua, _this, shape: Table| {
            let dimensions = dimensions_for_table(&shape)?;
            let delegate: Array<u8, Ix4> = Array4::ones(dimensions);
            Ok(Array4U8 { delegate })
        });
        methods.add_method("zeros", |_lua, _this, shape: Table| {
            let dimensions = dimensions_for_table(&shape)?;
            let delegate: Array<u8, Ix4> = Array4::zeros(dimensions);
            Ok(Array4U8 { delegate })
        });
    }
}

#[cfg(test)]
mod tests {
    use mlua::Lua;
    use std::error::Error;

    #[test]
    fn default() -> Result<(), Box<dyn Error>> {
        let lua = Lua::new();
        super::super::preload(&lua)?;
        let script = r#"
            local ndarray = require('tract.ndarray')
            return ndarray.Array4:u8():default({3, 4, 5, 6}):len()
        "#;
        let n: usize = lua.load(script).eval()?;
        assert_eq!(n, 360);
        Ok(())
    }

    #[test]
    fn from_shape_fn() -> Result<(), Box<dyn Error>> {
        let lua = Lua::new();
        super::super::preload(&lua)?;
        let script = r#"
            local ndarray = require('tract.ndarray')
            ndarray.Array4:u8():from_shape_fn({3, 4, 5, 6}, function(i, j, k, l)
                return i + j + k + l 
            end)
        "#;
        lua.load(script).exec()?;
        Ok(())
    }

    #[test]
    fn ones() -> Result<(), Box<dyn Error>> {
        let lua = Lua::new();
        super::super::preload(&lua)?;
        let script = r#"
            local ndarray = require('tract.ndarray')
            return ndarray.Array4:u8():ones({3, 4, 5, 6}):len()
        "#;
        let n: usize = lua.load(script).eval()?;
        assert_eq!(n, 360);
        Ok(())
    }

    #[test]
    fn zeros_with_bad_arg() -> Result<(), Box<dyn Error>> {
        let lua = Lua::new();
        super::super::preload(&lua)?;
        let script = r#"
            local ndarray = require('tract.ndarray')
            ndarray.Array4:u8():zeros({3, 4, 5})
        "#;
        let result = lua.load(script).exec();
        assert!(result.is_err());
        Ok(())
    }

    #[test]
    fn zeros() -> Result<(), Box<dyn Error>> {
        let lua = Lua::new();
        super::super::preload(&lua)?;
        let script = r#"
            local ndarray = require('tract.ndarray')
            ndarray.Array4:u8():zeros({3, 4, 5, 6})
        "#;
        lua.load(script).exec()?;
        Ok(())
    }
}
